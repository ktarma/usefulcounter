# Count "useful" lines and characters #

This is a simple Java program to count "useful" lines and characters of some code (currently configured for Java code).
These "useful" lines are all lines that aren't blank nor only consist of comments.
Also, this program can count "useful" characters.

This program should be run after code formatting, then there are no extra spaces.


For example, to show what kind of lines/characters are ignored:

```
#!java
package ee.motive.usefulcounter;

/**
 * This test class has 9 useful lines and 133 useful characters. 
 * These lines will not be counted.
 */
public class Test {
	private int integer = 0; // This comment will not be counted.

	// This line will not be counted.
	/*
	 * These lines will not be counted.
	 */
	private void someMethod() { /* Even this comment will not be counted. */
		if (integer < 1) {
			integer++;
		}
	}
}
```

For this example, the program output is:

```
#!text
File: src\ee\motive\usefulcounter\Test.java
Useful lines: 9 of Total lines: 19
Useful chars: 133
---
Total files: 1
Total useful lines: 9
```