package ee.motive.usefulcounter;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class DirectoryReader {

	private static int fileCount = 0;
	private static int lineCountUseful = 0;
	private static final String fileType = ".java";
	private static final boolean showChars = true;

	public static void main(String[] args) {
		try {
			Path startPath = Paths.get("src/ee/motive/usefulcounter/Test.java");
			Files.walkFileTree(startPath, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
					String filePath = file.toString();
					if (filePath.endsWith(fileType)) {
						System.out.println("File: " + filePath);
						fileCount++;
						lineCountUseful += getUsefulLines(filePath);
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException e) {
					return FileVisitResult.CONTINUE;
				}
			});
			System.out.println("---");
			System.out.println("Total files: " + fileCount);
			System.out.println("Total useful lines: " + lineCountUseful);
		} catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		}
	}

	private static int getUsefulLines(String path) {
		try {
			FileInputStream fileStream = new FileInputStream(path);
			DataInputStream dataStream = new DataInputStream(fileStream);
			BufferedReader reader = new BufferedReader(new InputStreamReader(dataStream));
			String line;
			int lineCount = 0, lineCountUseful = 0, charCountUseful = 0;
			while ((line = reader.readLine()) != null) {
				lineCount++;
				line = line.trim();
				if (!line.isEmpty()) {
					if (!line.startsWith("/") && !line.startsWith("*")) {
						lineCountUseful++;
						if (showChars) {
							// Useful characters are before the first comment.
							int indexOfComment1 = line.indexOf("//");
							int indexOfComment2 = line.indexOf("/*");
							int indexOfComment = -1;
							if (indexOfComment1 != -1 && indexOfComment2 != -1) {
								indexOfComment = Math.min(indexOfComment1, indexOfComment2);
							} else if (indexOfComment1 != -1) {
								indexOfComment = indexOfComment1;
							} else if (indexOfComment2 != -1) {
								indexOfComment = indexOfComment2;
							}
							if (indexOfComment != -1) {
								line = line.substring(0, indexOfComment).trim();
							}
							charCountUseful += line.length();
						}
					}
				}
			}
			System.out.println("Useful lines: " + lineCountUseful + " of Total lines: " + lineCount);
			if (showChars){
				System.out.println("Useful chars: " + charCountUseful);
			}
			dataStream.close();
			return lineCountUseful;
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
			return 0;
		}
	}
}