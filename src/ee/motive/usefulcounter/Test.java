package ee.motive.usefulcounter;

/**
 * This test class has 9 useful lines and 133 useful characters. 
 * These lines will not be counted.
 */
public class Test {
	private int integer = 0; // This comment will not be counted.

	// This line will not be counted.
	/*
	 * These lines will not be counted.
	 */
	private void someMethod() { /* Even this comment will not be counted. */
		if (integer < 1) {
			integer++;
		}
	}
}